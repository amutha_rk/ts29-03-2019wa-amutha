<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  background-color: black;
}

* {
  box-sizing: border-box;
}


.container {
  padding: 16px;
  background-color: white;
}

input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/ Overwrite default styles of hr /
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}


.registerbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity: 1;
}


a {
  color: dodgerblue;
}


</style>
</head>
<body>
<script>
function myFunction() {
  alert("I am an alert box!");
}
</script>


<form action="StudentInsertServlet" method="post">
  <div class="container">
    <h1>ADD STUDENT  DETAILS</h1>
  

    <label for="SStudentid"><b>Studentid</b></label>
    <input type="text"  name="Studentid" required>

    <label for="Studentname"><b>Studentname</b></label>
    <input type="text"  name="Studentname">

    <label for="Departmentid"><b>Departmentid</b></label>
    <input type="text" name="Departmentid">
    
    <label for="StudentPassword"><b>StudentPassword</b></label>
    <input type="text" name="StudentPassword">
    
    <hr>
    

    <!-- <button type="submit" class="registerbtn">ADD STUDENT</button> -->
    <button onclick="myFunction()">insert</button>


    
  </div>
  
  
</form>

</body>
</html>
</body>
</html>