<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student Attendance</title>
</head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

<script type="text/javascript">
	function box(id) {
		var f=document.getElementById(id);
		f.setAttribute("value",1);
	}
	function absentbox(id) {
		var f=document.getElementById(id);
		f.setAttribute("value",0);
	}


</script>
<style>
table {
  width:100%;
}
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
th, td {
  padding: 15px;
  text-align: left;
}
table tr:nth-child(even) {
  background-color: #eee;
}
table tr:nth-child(odd) {
 background-color: #fff;
}
table th {
  background-color: black;
  color: white;
}
</style>
<body>
<%@page  import="java.sql.*,java.util.*,java.util.Date" %>

<form action="AttendancetoDate">
<select name="hour" >
		 <option  value="1">1</option>
		 <option  value="2">2</option>
		 <option  value="3">3</option>
		 <option  value="4">4</option>
		 <option  value="5">5</option>
		 <option  value="6">6</option>
		 </select>
<table>
<BR>
<P style="color:green;">GREEN-PRESENT</P>
<P style="color:">RED-ABSENT</P>

<tr>
	<th>student id</th>
	<th>student name</th>
	<th>date</th>
 
	<th>status</th>
</tr>	
<%
 try{  
	   Class.forName("oracle.jdbc.driver.OracleDriver");  
	     
	   Connection con=DriverManager.getConnection
	   ("jdbc:oracle:thin:@localhost:1521:xe","system","12345");  
	   PreparedStatement stmt=con.prepareStatement("select * from student1 "); 
		 ResultSet rs=stmt.executeQuery(); 
		 int a=1; 
		 Date d=new Date();
		 while(rs.next()){
		 %>
		 <tr>
	 	 <td><%= rs.getString(1) %></td>
	 	 <input type="hidden" <%= "name="+rs.getString(1)   %> <%= "value="+rs.getString(1)   %>>
		 <td><%= rs.getString(2)%></td>
		 <input type="hidden" <%= "name="+rs.getString(2)   %> <%= "value="+rs.getString(2)   %>>
		 <td><%=  d   %></td>
	 
 
		 <td> <div style="background-color:green;float:left"><input   <%= "id="+a %> type="Radio"   <%= "name=attendance"+a %> value=0  onClick=box(id)  required></div><div style="background-color:red;float:left"> <input  <%= "id="+a %> type="Radio" <%= "name=attendance"+a %> value="0"   onClick=absentbox(id)  ></div></td>
		
		 </tr>
		 


		<%

		 a++;	}

	   con.close();  
	     
	   }catch(Exception e){
	    System.out.println(e);
	}  
 
%>
 
</table>
<input type="submit" value="submit">
 <button value="back"><a href="Report.jsp">back</a></button>
</form>




</body>
</html>