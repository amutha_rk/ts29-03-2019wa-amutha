<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  background-color: black;
}

* {
  box-sizing: border-box;
}


.container {
  padding: 16px;
  background-color: white;
}

input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/ Overwrite default styles of hr /
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}


.registerbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity: 1;
}


a {
  color: dodgerblue;
}


</style>
</head>
<body>
<script>
function myFunction() {
  alert("I am an alert box!");
}
</script>


<form action="FacultyInsertServlet" method="post">
  <div class="container">
    <h1>ADD FACULTY  DETAILS</h1>
  

    <label for="Facultyid"><b>Facultyid</b></label>
    <input type="text"  name="Facultyid" required>

    <label for="Facultyname"><b>Facultyname</b></label>
    <input type="text"  name="Facultyname">

    <label for="Departmentid"><b>Departmentid</b></label>
    <input type="text" name="Departmentid">
    
    <label for="Dateofbirth"><b>Dateofbirth</b></label>
    <input type="text" name="Dateofbirth">
    
    
    <label for="Subjectid"><b>Subjectid</b></label>
    <input type="text" name="Subjectid">
    
    <hr>
    

    <!-- <button type="submit" class="registerbtn">ADD FACULTY</button> -->
    <button onclick="myFunction()">insert</button>


    
  </div>
  
  
</form>

</body>
</html>
    