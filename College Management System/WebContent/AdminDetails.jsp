<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<body>
<style>
body {font-family: Arial, Helvetica, sans-serif;}
form {border: 3px solid #f1f1f1;}

input[type=text], input[type=password] {
  width: 30%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 30%;
}

button:hover {
  opacity: 0.8;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}


.container {
  padding: 16px;
}


/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}
</style>

<body>
<div style="margin-left: 450px;">

<form action="AttendancetoDate">
  <button value="StudentInsert"><a href="StudentInsert.jsp">StudentInsert</a></button><br>
  <button value="StudentDelete"><a href="StudentDelete.jsp">StudentDelete</a></button><br>
  <button value="FacultyInsert"><a href="FacultyInsert.jsp">FacultyInsert</a></button><br>
  <button value="FacultyDelete"><a href="FacultyDelete.jsp">FacultyDelete</a></button><br>
  <button value="DepartmentInsert"><a href="Department.jsp">DepartmentInsert</a></button><br>
  <button value="DepartmentDelete"><a href="DepartmentDelete.jsp">DepartmentDelete</a></button><br>
  <button value="SubjectInsert"><a href="SubjectInsert.jsp">SubjectInsert</a></button><br>
   <button value="SubjectDelete"><a href="SubjectDelete.jsp">SubjectDelete</a></button><br>
 <button value="back"><a href="index1.jsp">back</a></button>
</form>
</div>
</body>
</html>