<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<title>subjcet</title>
</head>
<style>
table {
  width:100%;
}
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
th, td {
  padding: 15px;
  text-align: left;
}
table tr:nth-child(even) {
  background-color: #eee;
}
table tr:nth-child(odd) {
 background-color: #fff;
}
table th {
  background-color: gray;
  color: black;
}
</style>

<body>

<th style="text-align:center;">Performance Details</th>
	<table style="width: 100%">
		<tr>
				<style >
				table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
 
}
</style>
			<th style="text-align:center;">subjectid</th>
			<th style="text-align:center;">Studentid</th>
			<th style="text-align:center;">studentname</th>
			<th style="text-align:center;">Semester</th>
			<th style="text-align:center;">Subjectname</th>
			<th style="text-align:center;">Subjectname</th>
			<th style="text-align:center;">Subjectname</th>
			<th style="text-align:center;">Subjectname</th>
			<th style="text-align:center;">Subjectname</th>
			<th style="text-align:center;">Subjectname</th>
			<th style="text-align:center;">Marks</th>
			<th style="text-align:center;">Grade</th>
		</tr>
		
		<th style="text-align:center;">4001</th>
			<th style="text-align:center;">1</th>
			<th style="text-align:center;">Mugesh</th>
			<th style="text-align:center;">1</th>
			<th style="text-align:center;">ControlSystem</th>
			<th style="text-align:center;">Professional Ethics</th>
			<th style="text-align:center;">Power System Dynamics</th>
			<th style="text-align:center;">Principles of Robotics</th>
			<th style="text-align:center;">Disaster Management</th>
			<th style="text-align:center;">Advanced Digital Signal Processing</th>
			<th style="text-align:center;">60</th>
			<th style="text-align:center;">E</th>
		</tr>
		<tr>
		<th style="text-align:center;">2</th>
			<th style="text-align:center;">Professional Ethics in Engineering</th>
			<th style="text-align:center;">Total Quality Management</th>
			<th style="text-align:center;">Advanced Digital Signal Processing</th>
			<th style="text-align:center;">Computer Aided Design of Electrical Apparatus</th>
			<th style="text-align:center;">VLSI Design</th>
			<th style="text-align:center;">Human Rights</th>
			<th style="text-align:center;">70</th>
			<th style="text-align:center;">D</th>
			</tr>
			<tr>
			<th style="text-align:center;">3</th>
			<th style="text-align:center;">Introduction to Electronics</th>
			<th style="text-align:center;">Introduction to Material Science and Engineering</th>
			<th style="text-align:center;">Introduction to Bioscience and Technology</th>
			<th style="text-align:center;">Probability Statistics and Stochastic Processes (Mathematics – 3)</th>
			<th style="text-align:center;">Signals and Systems</th>
			<th style="text-align:center;">Signals and Systems Laboratory</th>
			<th style="text-align:center;">75</th>
			<th style="text-align:center;">D</th>
			</tr>
			<tr>
			<th style="text-align:center;">3</th>
			<th style="text-align:center;">Introduction to Electronics</th>
			<th style="text-align:center;">Introduction to Material Science and Engineering</th>
			<th style="text-align:center;">Introduction to Bioscience and Technology</th>
			<th style="text-align:center;">Probability Statistics and Stochastic Processes (Mathematics – 3)</th>
			<th style="text-align:center;">Signals and Systems</th>
			<th style="text-align:center;">Signals and Systems Laboratory</th>
			<th style="text-align:center;">75</th>
			<th style="text-align:center;">D</th>
			</tr>
			<tr>
		<th style="text-align:center;">4</th>
			<th style="text-align:center;">Environmental Science Technology and Management</th>
			<th style="text-align:center;">Analog Electronics Circuits</th>
			<th style="text-align:center;">Digital Electronics Circuits</th>
			<th style="text-align:center;">Network Theory</th>
			<th style="text-align:center;">Analog Electronics Circuits Laboratory</th>
			<th style="text-align:center;">Digital Electronics Circuits Laboratory</th>
			<th style="text-align:center;">80</th>
			<th style="text-align:center;">B</th>
			</tr>
			<th style="text-align:center;">5</th>
			<th style="text-align:center;">Electric Machines</th>
			<th style="text-align:center;">Communication Engineering</th>
			<th style="text-align:center;">Measurement and Electronic Instruments</th>
			<th style="text-align:center;">Electromagnetic Engineering</th>
			<th style="text-align:center;">Communication Engineering  Laboratory</th>
			<th style="text-align:center;">Measurement and Electronic Instruments Laboratory</th>
			<th style="text-align:center;">85</th>
			<th style="text-align:center;">B</th>
			</tr>
			</tr>
			<th style="text-align:center;">6</th>
			<th style="text-align:center;">Control Systems</th>
			<th style="text-align:center;">Digital Signal Processing</th>
			<th style="text-align:center;">Power Electronics</th>
			<th style="text-align:center;">Control Systems Laboratory</th>
			<th style="text-align:center;">Digital Signal Processing Laboratory</th>
			<th style="text-align:center;">Power Electronics Laboratory</th>
			<th style="text-align:center;">90</th>
			<th style="text-align:center;">A</th>
			</tr>
			</tr>
			<th style="text-align:center;">7</th>
			<th style="text-align:center;">Semiconductor Devices</th>
			<th style="text-align:center;">Computational Intelligence</th>
			<th style="text-align:center;">Speech Signal Processing</th>
			<th style="text-align:center;">Energy Storage Systems</th>
			<th style="text-align:center;">Computer Organization and Architecture</th>
			<th style="text-align:center;">Compiler Design</th>
			<th style="text-align:center;">70</th>
			<th style="text-align:center;">D</th>
			</tr>
			<th style="text-align:center;">8</th>
			<th style="text-align:center;">Electric Energy Generation, Utilization and Conservation</th>
			<th style="text-align:center;">Elective – IV</th>
			<th style="text-align:center;">Elective – V</th>
			<th style="text-align:center;">Project Work</th>
			<th style="text-align:center;">95</th>
			<th style="text-align:center;">S</th>
			</tr>
		
		<div>
			<a href="performance.jsp"><button class="btn btn-success" style="width: 200px;margin-top: 10px">Submit
			</button></a>
			</div>
</body>
</html> 