<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">


<title>NMA^2</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style >

body {
  font-family: Arial, Helvetica, sans-serif;
}

.navbar {
  overflow: hidden;
  background-color: #333;
}

.navbar a {
  float: left;
  font-size: 16px;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

.dropdown {
  float: left;
  overflow: hidden;
}

.dropdown .dropbtn {
  font-size: 16px;  
  border: none;
  outline: none;
  color: white;
  padding: 14px 16px;
  background-color: inherit;
  font-family: inherit;
  margin: 0;
}

.navbar a:hover, .dropdown:hover .dropbtn {
  background-color: red;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {
  background-color: #ddd;
}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>



<body>

<!-- Navbar (sit on top) -->

         <div>             
                       
     <a href="#home" class="w3-bar-item w3-button"><b>NMA^2 </b>College of Engineering</a>
   
   </div>

      <div class="dropdown">
    <button class="dropbtn">Admin
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="Admin.jsp">Add Student</a>
      <a href="Admin1.jsp">DeleteStudent</a>
      <a href="Admin.jsp">Add Faculty</a>
      <a href="Admin1.jsp">Delete Faculty</a>
      <a href="Admin.jsp">Add Department</a>
      <a href="Admin1.jsp">Delete Department</a>
      <a href="Admin.jsp">Add Subject</a>
      <a href="Admin1.jsp">Delete Subject</a> 
    
  </div>

  <div class="dropdown">
    <button class="dropbtn">Attendance
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="StaffLogin.jsp">Faculty login</a>
      <a href="StudentAttendancepage.jsp">Attendance</a>
      <a href="Attendance.jsp">Percentage</a>
    </div>
  </div>

  <div class="w3-right w3-hide-small">
     <div>
       
      <a href="Academic.html" class=       "w3-bar-item w3-button">Academic</a>
      
        <a href="Research.html" class="w3-bar-item w3-button">Research</a>
         <a href="Placements.html" class="w3-bar-item w3-button">Placements</a>
      <a href="#contact" class="w3-bar-item w3-button">Contact</a>
       <a href="Login.jsp" class="w3-bar-item w3-button">login</a>
    </div>
</div>

<!-- Header -->
<header class="w3-display-container w3-content w3-wide" style="max-width:1500px;" id="home">
  <img class="w3-image" src="VIT_College_Infra.jpg" alt="Architecture" width="1500" height="800">
  <div class="w3-display-middle w3-margin-top w3-center">
    <h1 class="w3-xxlarge w3-text-white"><span class="w3-padding w3-black w3-opacity-min"><b>NMA^2 </b></span> <span class="w3-hide-small w3-text-light-grey">College of Engineering</span></h1>
  </div>
</header>

<!-- Page content -->
<div class="w3-content w3-padding" style="max-width:1564px">

  <!-- Project Section -->
  <div class="w3-container w3-padding-32" id="projects">
    <h3 class="w3-border-bottom w3-border-light-grey w3-padding-16">Projects</h3>
  </div>
</div>
  <div class="w3-row-padding">
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-display-container">
        <div class="w3-display-topleft w3-black w3-padding">Department</div>
        <img src="M.Kumarasamy College of Engineering, NAAC Accredited Autonomous Institution, Karur, Tamilnadu_files\1543664023.jpg" alt="Department" style="width:100%">
      </div>
	  </div>
    </div>
	
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-display-container">
        <div class="w3-display-topleft w3-black w3-padding">Class</div>
        <img src="Collage_students_image.jpg" alt="Class" style="width:100%">
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-display-container">
        <div class="w3-display-topleft w3-black w3-padding">Labratory</div>
        <img src="M.Kumarasamy College of Engineering, NAAC Accredited Autonomous Institution, Karur, Tamilnadu_files\1544092020.jpg" alt="Labratory" style="width:100%">
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-display-container">
        <div class="w3-display-topleft w3-black w3-padding">Sport</div>
        <img src="M.Kumarasamy College of Engineering, NAAC Accredited Autonomous Institution, Karur, Tamilnadu_files\1544007442.jpg" alt="Sport" style="width:100%">
      </div>
    </div>
  </div>

  <div class="w3-row-padding">
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-display-container">
        <div class="w3-display-topleft w3-black w3-padding">Cultural</div>
        <img src="images (4).jpg" alt="Cultural" style="width:99%">
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-display-container">
        <div class="w3-display-topleft w3-black w3-padding">Staffs</div>
        <img src="download (1).jpg" alt="Staffs" style="width:99%">
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-display-container">
        <div class="w3-display-topleft w3-black w3-padding">Gratuation</div>
        <img src="images (6).jpg" alt="Gratuation" style="width:99%">
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <div class="w3-display-container">
        <div class="w3-display-topleft w3-black w3-padding">Infrastructure</div>
        <img src=" images (1).jpg" alt="Infrastructure" style="width:99%">
      </div>
    </div>
  </div>

 

  
  
<!-- Image of location/map -->
<div class="w3-container">
  <img src=" map.jpg" class="w3-image" style="width:100%">
</div>

<!-- End page content -->



<!-- Footer -->
<footer class="w3-center w3-black w3-padding-16">
  <p><a href="https://www.w3schools.com/w3css/default.asp" title="home Page" target="_blank" class="w3-hover-text-green">Home Page</a></p>
</footer>

</body>
</html>