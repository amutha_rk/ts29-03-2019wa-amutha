package bean;

public class FacultyInsertBean {
	String Facultyid ;
	String Facultyname;
    String 	Departmentid;
    String 	Dateofbirth;
    String Subjectid;

	public String getFacultyid() {
		return Facultyid;
	}
	public void setFacultyid(String facultyid) {
		Facultyid = facultyid;
	}
	public String getFacultyname() {
		return Facultyname;
	}
	public void setFacultyname(String facultyname) {
		Facultyname = facultyname;
	}
	public String getDepartmentid() {
		return Departmentid;
	}
	public void setDepartmentid(String departmentid) {
		Departmentid = departmentid;
	}
	public String getDateofbirth() {
		return Dateofbirth;
	}
	public void setDateofbirth(String dateofbirth) {
		Dateofbirth = dateofbirth;
	}
	public String getSubjectid() {
		return Subjectid;
	}
	public void setSubjectid(String subjectid) {
		Subjectid = subjectid;
	}
	
}
