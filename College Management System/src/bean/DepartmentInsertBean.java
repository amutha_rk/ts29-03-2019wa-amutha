package bean;

public class DepartmentInsertBean {
	String Departmentid ;
	String Departmentname;
    String 	HODname;
    String Studentid;
	public String getDepartmentid() {
		return Departmentid;
	}
	public void setDepartmentid(String departmentid) {
		Departmentid = departmentid;
	}
	public String getDepartmentname() {
		return Departmentname;
	}
	public void setDepartmentname(String departmentname) {
		Departmentname = departmentname;
	}
	public String getHODname() {
		return HODname;
	}
	public void setHODname(String hODname) {
		HODname = hODname;
	}
	public String getStudentid() {
		return Studentid;
	}
	public void setStudentid(String studentid) {
		Studentid = studentid;
	}
	

}
