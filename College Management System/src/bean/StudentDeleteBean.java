package bean;

public class StudentDeleteBean {
	String Studentid;
	String Studentname;
    String Departmentid;
    String StudentPassword;
	 
	public String getStudentid() {
		return Studentid;
	}
	public void setStudentid(String studentid) {
		Studentid = studentid;
	}
	public String getStudentname() {
		return Studentname;
	}
	public void setStudentname(String studentname) {
		Studentname = studentname;
	}
	public String getDepartmentid() {
		return Departmentid;
	}
	public void setDepartmentid(String departmentid) {
		Departmentid = departmentid;
	}
	public String getStudentPassword() {
		return StudentPassword;
	}
	public void setStudentPassword(String studentPassword) {
		StudentPassword = studentPassword;
	}
}
    


