package bean;

public class SubjectDeleteBean {
	String Subjectid;
	String Subjectname;
	String Departmentid;
	public String getSubjectid() {
		return Subjectid;
	}
	public void setSubjectid(String subjectid) {
		Subjectid = subjectid;
	}
	public String getSubjectname() {
		return Subjectname;
	}
	public void setSubjectname(String subjectname) {
		Subjectname = subjectname;
	}
	public String getDepartmentid() {
		return Departmentid;
	}
	public void setDepartmentid(String departmentid) {
		Departmentid = departmentid;
	}
	

}
