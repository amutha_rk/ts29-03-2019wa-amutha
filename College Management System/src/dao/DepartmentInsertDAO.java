package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import Connection.connectionDB;
import bean.DepartmentInsertBean;
import bean.StudentInsertBean;

public class DepartmentInsertDAO {
	public String InsertData(DepartmentInsertBean insert) {
		String Departmentid=insert.getDepartmentid();
		String Departmentname =insert.getDepartmentname();
		String HODname=insert.getHODname();
		String Studentid=insert.getStudentid();
		
		System.out.println("------------->"+Departmentname);
		Connection con = null;
	 
	 
		 
		try
		 {
		 con = connectionDB.createConnection(); 
		 PreparedStatement stmt=con.prepareStatement("insert into Department values(?,?,?,?)");
		 stmt.setString(1, Departmentid);
		 stmt.setString(2,Departmentname );
		 stmt.setString(3,HODname);
		 stmt.setString(4,Studentid);
		 
		 	
		 int i=stmt.executeUpdate();  
			System.out.println(i+" record inserted");  
			  
			con.close();  
		 }
		 catch(SQLException e)
		 {
		 e.printStackTrace();
		 }
		 return "Invalid user credentials"; 
	}

}







