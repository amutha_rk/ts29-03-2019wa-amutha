package contollerdb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.DepartmentInsertBean;
import bean.StudentInsertBean;
import dao.DepartmentInsertDAO;
import dao.StudentInsertDAO;


@WebServlet("/DepartmentInsertServlet")
public class DepartmentInsertServlet extends HttpServlet {
private static final long serialVersionUID = 1L;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String Departmentid=request.getParameter("Departmentid");
		 String Departmentname=request.getParameter("Departmentname");
		 String HODname=request.getParameter("HODname");
		 String Studentid=request.getParameter("Studentid");
		 System.out.println("Student from servlet"+Departmentname);
		 DepartmentInsertBean insert=new DepartmentInsertBean();
		 
		 insert.setDepartmentid(Departmentid);
		 insert.setDepartmentname(Departmentname);
		 insert.setHODname(HODname);
		 insert.setStudentid(Studentid);
		  
		 DepartmentInsertDAO insertDao = new DepartmentInsertDAO(); 
		 insertDao.InsertData(insert);
		 
		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:Tomato;\"> Inserted Succesfully </h1>");	           
		 writer.close();
	  
	}

}

	