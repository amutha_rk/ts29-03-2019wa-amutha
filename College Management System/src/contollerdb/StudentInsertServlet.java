package contollerdb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.AdminBean;
import bean.StudentInsertBean;
import dao.StudentInsertDAO;


@WebServlet("/StudentInsertServlet")
public class StudentInsertServlet extends HttpServlet implements Servlet {
	

private static final long serialVersionUID = 1L;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String Studentid=request.getParameter("Studentid");
		 String Studentname=request.getParameter("Studentname");
		 String Departmentid=request.getParameter("Departmentid");
		 String StudentPassword=request.getParameter("StudentPassword");
		 System.out.println("Student from servlet"+Studentname);
		 StudentInsertBean insert=new StudentInsertBean();
		 
		 insert.setStudentid(Studentid);
		 insert.setStudentame(Studentname);
		 insert.setDepartmentid(Departmentid);
		 insert.setStudentPassword(StudentPassword);
		  
		 StudentInsertDAO insertDao = new StudentInsertDAO(); 
		 insertDao.InsertData(insert);
		 
		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:Tomato;\"> Inserted Succesfully </h1>");	           
		 writer.close();
	  
	}

}