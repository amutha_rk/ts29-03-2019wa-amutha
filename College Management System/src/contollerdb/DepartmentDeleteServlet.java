package contollerdb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.DepartmentDeleteBean;
import bean.FacultyDeleteBean;
import dao.DepartmentDeleteDAO;
import dao.FacultyDeleteDAO;


@WebServlet("/DepartmentDeleteServlet")
public class DepartmentDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String Departmentid=request.getParameter("Departmentid");
		 String Departmentname=request.getParameter("Departmentname");
		 String HODname=request.getParameter("HODname");
		 String Studentid=request.getParameter("Studentid");
		 
		 System.out.println("Student from servlet"+Departmentname);
		 
		
		 

		 DepartmentDeleteBean delete=new DepartmentDeleteBean();
		 
		 delete.setDepartmentid(Departmentid);
		 delete.setDepartmentname(Departmentname);
		 delete.setHODname(HODname);
		 delete.setStudentid(Studentid);
		
		
		  
		 DepartmentDeleteDAO deleteDao = new DepartmentDeleteDAO(); 
		 deleteDao.delete(delete);
		 
		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:blue;\"> deleted Succesfully </h1>");	           
		 writer.close();
	}
}



       


	