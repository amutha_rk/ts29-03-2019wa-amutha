package contollerdb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.FacultyDeleteBean;
import bean.StudentDeleteBean;
import dao.FacultyDeleteDAO;
import dao.StudentDeleteDAO;


@WebServlet("/FacultyDeleteServlet")
public class FacultyDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String Facultyid=request.getParameter("Facultyid");
		 String Facultyname=request.getParameter("Facultyname");
		 String Departmentid=request.getParameter("Departmentid");
		 String Dateofbirth=request.getParameter("Dateofbirth");
		 String Subjectid=request.getParameter("Subjectid");
		 System.out.println("Student from servlet"+Facultyname);
		 
		
		 

		 FacultyDeleteBean delete=new FacultyDeleteBean();
		 
		 delete.setFacultyid(Facultyid);
		 delete.setFacultyname(Facultyname);
		 delete.setDepartmentid(Departmentid);
		 delete.setDateofbirth(Dateofbirth);
		 delete.setSubjectid(Subjectid);
		  
		
		  
		 FacultyDeleteDAO deleteDao = new FacultyDeleteDAO(); 
		 deleteDao.delete(delete);
		 
		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:blue;\"> deleted Succesfully </h1>");	           
		 writer.close();
	}
}



       
    