package contollerdb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.StudentDeleteBean;
import dao.StudentDeleteDAO;


@WebServlet("/StudentDeleteServlet")
public class StudentDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String Studentid=request.getParameter("Studentid");
		 String Studentname=request.getParameter("Studentname");
		 String Departmentid=request.getParameter("Departmentid");
		 String StudentPassword=request.getParameter("StudentPassword");
		 System.out.println("Student from servlet"+Studentname);
		 
		
		 

		 StudentDeleteBean delete=new StudentDeleteBean();
		 
		 delete.setStudentid(Studentid);
		 delete.setStudentname(Studentname);
		 delete.setDepartmentid(Departmentid);
		 delete.setStudentPassword(StudentPassword);
		  
		
		  
		StudentDeleteDAO deleteDao = new StudentDeleteDAO(); 
		 deleteDao.delete(delete);
		 
		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:blue;\"> deleted Succesfully </h1>");	           
		 writer.close();
	}
}


