package contollerdb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.FacultyInsertBean;
import bean.StudentInsertBean;
import dao.FacultyInsertDAO;
import dao.StudentInsertDAO;


@WebServlet("/FacultyInsertServlet")
public class FacultyInsertServlet extends HttpServlet {
private static final long serialVersionUID = 1L;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String Facultyid=request.getParameter("Facultyid");
		 String Facultyname=request.getParameter("Facultyname");
		 String Departmentid=request.getParameter("Departmentid");
		 String Dateofbirth=request.getParameter("Dateofbirth");
		 String Subjectid=request.getParameter("Subjectid");
		 System.out.println("Student from servlet"+Facultyname);
		 
		 FacultyInsertBean insert=new FacultyInsertBean();
		 
		 insert.setFacultyid(Facultyid);
		 insert.setFacultyname(Facultyname);
		 insert.setDepartmentid(Departmentid);
		 insert.setDateofbirth(Dateofbirth);
		 insert.setSubjectid(Subjectid);
		  
		 FacultyInsertDAO insertDao = new FacultyInsertDAO(); 
		 insertDao.InsertData(insert);
		 
		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:Tomato;\"> Inserted Succesfully </h1>");	           
		 writer.close();
	  
	}

}

	