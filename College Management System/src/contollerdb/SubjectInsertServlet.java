package contollerdb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.StudentInsertBean;
import bean.SubjectInsertBean;
import dao.StudentInsertDAO;
import dao.SubjectInsertDAO;


@WebServlet("/SubjectInsertServlet")
public class SubjectInsertServlet extends HttpServlet implements Servlet {
private static final long serialVersionUID = 1L;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String Subjectid=request.getParameter("Subjectid");
		 String Subjectname=request.getParameter("Subjectname");
		 String Departmentid=request.getParameter("Departmentid");
		 
		 System.out.println("Student from servlet"+Subjectname);
		 SubjectInsertBean insert=new SubjectInsertBean();
		 
		 insert.setSubjectid(Subjectid);
		 insert.setSubjectname(Subjectname);
		 insert.setDepartmentid(Departmentid);
		 
		  
		 SubjectInsertDAO insertDao = new SubjectInsertDAO(); 
		 insertDao.InsertData(insert);
		 
		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:Tomato;\"> Inserted Succesfully </h1>");	           
		 writer.close();
	  
	}

}
	

s
