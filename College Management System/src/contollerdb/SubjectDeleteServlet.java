package contollerdb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.StudentDeleteBean;
import bean.SubjectDeleteBean;
import dao.StudentDeleteDAO;
import dao.SubjectDeleteDAO;


@WebServlet("/SubjectDeleteServlet")
public class SubjectDeleteServlet extends HttpServlet implements Servlet {
	private static final long serialVersionUID = 1L;
	 
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String Subjectid=request.getParameter("Subjectid");
		 String Subjectname=request.getParameter("Subjectname");
		 String Departmentid=request.getParameter("Departmentid");
		 
		 System.out.println("Student from servlet"+Subjectname);
		 
		
		 

		 SubjectDeleteBean delete=new SubjectDeleteBean();
		 
		 delete.setSubjectid(Subjectid);
		 delete.setSubjectname(Subjectname);
		 delete.setDepartmentid(Departmentid);
		 
		  
		
		  
		 SubjectDeleteDAO deleteDao = new SubjectDeleteDAO(); 
		 deleteDao.delete(delete);
		 
		 response.setContentType("text/html");
		 PrintWriter writer =  response.getWriter();
		 writer.println("<h1 style=\"color:blue;\"> deleted Succesfully </h1>");	           
		 writer.close();
	}
}


